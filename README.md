# GitLab Meetup CW13, 2020-03-24

Presentation by [Michael Friedrich](https://gitlab.com/dnsmichi) including exercises: [Make your life as developer easier with CI/CD and the Web IDE](https://docs.google.com/presentation/d/1scYkmV4Xdfj-8iwwpEiLCe0vBfpAdrL5pyA2w1Fgnf0/edit?usp=sharing)

